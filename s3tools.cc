#include "s3tools.h"
#include <syslog.h>
#include <pwd.h>
#include <sys/types.h>
#include <dirent.h>
#include <fuse.h>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <time.h>

std::string G_CONFIG_FILE     = "/etc/stutz-s3fs-config";
std::string G_USERS_CONFIG    = "/etc/stutz-s3fs-users";
std::string G_WORK_DIR_PREFIX = "/var/cache/stutz-s3fs/";
std::map<std::string, std::string> G_USER_PROFILES = std::map<std::string, std::string>();
std::string G_BUCKET_NAME = std::string(); 
std::string G_MOUNTPOINT = std::string();
S3Queue G_S3_QUEUE = S3Queue();
std::map<pid_t, std::set<std::string> > G_OPEN_FILES = std::map<pid_t, std::set<std::string> >();

char * get_username(void)
{
  struct fuse_context * f_context = fuse_get_context();
  struct passwd * pw = getpwuid(f_context->uid);
  return pw->pw_name;
}

int access_time_diff(const char * path)
{
  time_t current_time;
  struct stat st_path;
  
  stat(path, &st_path);
  current_time = time(NULL);
  
  return difftime(current_time, st_path.st_atime);
}

void init_aws_user_profiles(void)
{
  // fill a map with usernames in system and aws profiles
  FILE * fh = fopen(G_USERS_CONFIG.c_str(), "r");
  if (!fh) {
    syslog(LOG_INFO, "Could not open configuration file %s\n", 
           G_USERS_CONFIG.c_str());
    return;
  }
  
  char line[AWS_MAX_FILENAME_LEN+64];
  while (fgets(line, AWS_MAX_FILENAME_LEN+64, fh) != NULL) {
    char username[512], profile[512];
    sscanf(line, "%[^=]= %[^\n]s", username, profile);
    G_USER_PROFILES.insert(std::make_pair(std::string(username), 
                                          std::string(profile)));
  }
  
  fclose(fh);
}

void create_cache_work_dir(void) 
{
  std::string work_dir = G_WORK_DIR_PREFIX + G_MOUNTPOINT; 
  /* Check if it exists. If not, create it */
  if (opendir(work_dir.c_str()) == NULL) {
    if (errno == ENOENT) { /* Directory does not exists */
      syslog(LOG_INFO, "%s directory does not exists.\n", work_dir.c_str());
      
      if (mkdir(work_dir.c_str(), S_IRWXU | S_IRWXG | S_IRWXO) != 0) { 
        /* Error creating the cache directory */
        syslog(LOG_INFO, "Error creating directory %s\n", work_dir.c_str());
        exit(1);
      }         
    } else if(errno == EACCES) { /* Permission denied */
      std::cerr << "Permission denied on " << work_dir << std::endl;
      exit(1);
    } else if(errno == ENOTDIR) { /* It is not a directory */
      std::cerr << work_dir << " is not a directory" << std::endl;
      exit(1);
    }
  }
}

void clean_cache_work_dir(void)
{
  //TODO clean_cache_work_dir
}

int evaluate_output(const char *line, const char *aws_bucket_path)
{
  int ret_val = 0;
  if (strcasestr(line, "NoSuchKey") != NULL) { 
    /* Does not exists locally or remotely and it is not creating it ¿? */
    syslog(LOG_INFO, "[Error] File %s does not exists\n", 
            aws_bucket_path);
    ret_val = -ENOENT;
  } else if (strcasestr(line, "Forbidden") != NULL) { /* Not allowed */
    syslog(LOG_INFO, "[Error] Access denied on file %s for user %s\n", 
            aws_bucket_path, get_username());
    ret_val = -EACCES;
  } else if (strcasestr(line, "AccessDenied") != NULL) { /* Not allowed */
    syslog(LOG_INFO, "[Error] Access denied on file %s for user %s\n", 
            aws_bucket_path, get_username());
    ret_val = -EACCES;
  } else if (strcasestr(line, "error") != NULL) { /* unknown error */
    syslog(LOG_INFO, "[Error] Unknown error opening file %s\n", 
            aws_bucket_path);
    ret_val = -1;
  }
  
  return ret_val;
}

int download_file(const char *path) 
{
  int ret_val = 0;
  syslog(LOG_INFO, "I am going to download %s from AWS\n", path);
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  std::string aws_bucket_path = G_BUCKET_NAME + path; 
  std::string aws_cmd = "aws s3 cp " + aws_bucket_path + " " + 
                        full_cache_path + 
                        " --profile " + 
                        G_USER_PROFILES[std::string(get_username())];
                        
  syslog(LOG_INFO, aws_cmd.c_str());
  
  FILE * aws_cmd_fh = popen(aws_cmd.c_str(), "r");
  char line[AWS_MAX_FILENAME_LEN+64];
  if (fgets(line, AWS_MAX_FILENAME_LEN+64, aws_cmd_fh) != NULL) {
    ret_val = evaluate_output(line, aws_bucket_path.c_str());
  }
  
  pclose(aws_cmd_fh);
  /* Touch the access time for the file */
  time_t current_time = time(NULL);
  std::string touch_cmd = "touch -a -d \"" 
                          + std::string(ctime(&current_time)) 
                          +"\" " + full_cache_path;
  system(touch_cmd.c_str());
  return ret_val;
}

void upload_file(const char *localfile, const char *remotefile, 
                 const char * username, bool update_mod_time)
{
  syslog(LOG_INFO, "I am going to upload %s to AWS\n", localfile);
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + localfile;
  std::string aws_bucket_path = G_BUCKET_NAME + remotefile; 
  std::string profile = G_USER_PROFILES[std::string(username)];
  std::string aws_cmd = "aws s3 cp " + full_cache_path + " " + 
                        aws_bucket_path + 
                        " --profile " + profile;
                        
  syslog(LOG_INFO, aws_cmd.c_str());
  
  FILE * aws_cmd_fh = popen(aws_cmd.c_str(), "r");
  char line[AWS_MAX_FILENAME_LEN+64];
  if (fgets(line, AWS_MAX_FILENAME_LEN+64, aws_cmd_fh) != NULL) {
    int ret_val = evaluate_output(line, remotefile);
    if (ret_val == -EACCES) {
      /*TODO write log permission denied */
    } else if (ret_val == 0 && update_mod_time) {
      /* Get the modication date from the just uploaded file */
//       aws_cmd = "aws s3 ls " + aws_bucket_path +
//                             " --profile " + profile;
//       FILE * aws_cmd_fh2 = popen(aws_cmd.c_str(), "r");
//       if (fgets(line, AWS_MAX_FILENAME_LEN+64, aws_cmd_fh2)) {
//         char date_[15], time_[15];
//         sscanf(line, "%s %s %*s %*s\n", date_, time_);
//         std::string touch_cmd = "touch -m -d \"" + std::string(date_) + " " 
//                                 + std::string(time_) +"\" " + full_cache_path;
//         
//         system(touch_cmd.c_str()); /* This will create with RW permission */
//       }
//       pclose(aws_cmd_fh2);
    }
  }
  
  pclose(aws_cmd_fh);
}


int aws_has_file(const char *path, bool *folder) 
{
  int ret_val = 0;
  syslog(LOG_INFO, "I am going to check the existance of %s in AWS\n", path);
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  std::string aws_bucket_path = G_BUCKET_NAME + path; 
  std::string aws_cmd = "aws s3 ls " + aws_bucket_path +
                        " --profile " + 
                        G_USER_PROFILES[std::string(get_username())];
                        
  syslog(LOG_INFO, aws_cmd.c_str());
  
  FILE * aws_cmd_fh = popen(aws_cmd.c_str(), "r");
  char line[AWS_MAX_FILENAME_LEN+64];
  if (fgets(line, AWS_MAX_FILENAME_LEN+64, aws_cmd_fh) != NULL) {
    ret_val = evaluate_output(line, aws_bucket_path.c_str());
    
    /* Cleaning the spaces at the beggining */
    size_t line_len = strlen(line);
    unsigned i;
    for (i=0; i<line_len; i+=1) {
      if(!isspace(line[i])) break;
    }
    memmove(line, line+i, line_len-i);
    *folder = (strncmp(line, "PRE", 3) == 0); /* It is a directory */
  } else {
    ret_val = -ENOENT;
  }
  
  pclose(aws_cmd_fh);
  return ret_val;
}

bool outdated(const char * path, const char * username, time_t last_modified)
{
  bool ret_val = false;
  std::string aws_bucket_path = G_BUCKET_NAME + path; 
  std::string aws_cmd = "aws s3 ls " + aws_bucket_path + " --profile " +
                        G_USER_PROFILES[std::string(username)];
                        
  syslog(LOG_INFO, "*outdated* %s\n ",aws_cmd.c_str());
  
  FILE * aws_cmd_fh = popen(aws_cmd.c_str(), "r");
  char line[AWS_MAX_FILENAME_LEN+64];
  
  if(fgets(line, AWS_MAX_FILENAME_LEN+64, aws_cmd_fh) != NULL)
  {
    /* Keep the date and the filename */
    char date_[15], time_[15];
    sscanf(line, "%s %s %*s %*s\n", date_, time_);
    std::string tmp_tm_str = std::string(const_cast<const char *>(date_)) 
                              + " " +
                              std::string(const_cast<const char *>(time_));
    // Some magic going on
    struct tm tm_aws, tm_local;
    strptime(tmp_tm_str.c_str(), "%Y-%m-%d %H:%M:%S", &tm_aws);
    localtime_r(&last_modified, &tm_local);
    tm_aws.tm_isdst = tm_local.tm_isdst;
              
    // era > 0
    ret_val = difftime(mktime(&tm_aws), mktime(&tm_local)) > 
              (TIME_TO_SLEEP + CACHE_MAX_SECS);
  }
  
  pclose(aws_cmd_fh);
  
  return ret_val;
}



/* file_needs_update function returns TRUE if the file in AWS has been updated
 * since the last time we downloaded the file */
bool file_needs_update(const char *path) 
{
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  /* First check if the file older than CACHE_MAX_SECS */
  if (access_time_diff(full_cache_path.c_str()) > CACHE_MAX_SECS) {
    /* Check if the file exists in AWS */
    std::string aws_bucket_path = G_BUCKET_NAME + path; 
    std::string aws_cmd = "aws s3 ls " + aws_bucket_path + " --profile " +
                          G_USER_PROFILES[std::string(get_username())];
                          
    syslog(LOG_INFO, "*file_needs_update* %s\n ",aws_cmd.c_str());
    
    FILE * aws_cmd_fh = popen(aws_cmd.c_str(), "r");
    char line[AWS_MAX_FILENAME_LEN+64];
    
    if(fgets(line, AWS_MAX_FILENAME_LEN+64, aws_cmd_fh) != NULL)
    {
      /* Keep the date and the filename */
      char date_[15], time_[15];
      sscanf(line, "%s %s %*s %*s\n", date_, time_);
      std::string tmp_tm_str = std::string(const_cast<const char *>(date_)) 
                               + " " +
                               std::string(const_cast<const char *>(time_));
      struct tm tm_;
      strptime(tmp_tm_str.c_str(), "%Y-%m-%d %H:%M:%S", &tm_);
      time_t aws_time = mktime(&tm_);
      struct stat st_path;
      stat(full_cache_path.c_str(), &st_path);
      
      /* If the diff is positive means that the file in 
       * AWS is a younger file i.e. it has been updated 
       * since last download*/
      pclose(aws_cmd_fh);
      
      syslog(LOG_INFO, "*file_needs_update* aws_time: %ld | st_mtime: %ld\n",
             aws_time, st_path.st_mtime);
      
      return difftime(aws_time, st_path.st_mtime) > 0; 
    }
    pclose(aws_cmd_fh);
  }
  return false;
}

std::vector<std::string> files_in_aws_path(const char * path, const char * username)
{
  std::vector<std::string> files;
  std::string aws_path = G_BUCKET_NAME + path;
  syslog(LOG_INFO, "I am going to check files in %s\n", aws_path.c_str()); 
  std::string aws_cmd = "aws s3 ls " + aws_path +
                        " --profile " + 
                        G_USER_PROFILES[std::string(username)];
                        
  syslog(LOG_INFO, aws_cmd.c_str());
  
  FILE * aws_cmd_fh = popen(aws_cmd.c_str(), "r");
  char line[AWS_MAX_FILENAME_LEN+64];
  while (fgets(line, AWS_MAX_FILENAME_LEN+64, aws_cmd_fh) != NULL) {    
    /* Cleaning the spaces at the beggining */
    size_t line_len = strlen(line);
    unsigned i;
    for (i=0; i<line_len; i+=1) {
      if(!isspace(line[i])) break;
    }
    memmove(line, line+i, line_len-i);
    if (strncmp(line, "PRE", 3) != 0) {
      char temporal_name[AWS_MAX_FILENAME_LEN];
      sscanf(line, "%*s %*s %*s %[^\n]s", temporal_name);
      files.push_back(std::string(temporal_name));
    }
  } 
  
  pclose(aws_cmd_fh);
  return files;
}