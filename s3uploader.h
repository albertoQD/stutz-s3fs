#ifndef STUTZ_S3FS_S3UPLOADER_H
#define STUTZ_S3FS_S3UPLOADER_H

#include <string>
#include <mutex>
#include <thread>

class S3Uploader
{
public:
  S3Uploader(const std::string /* path */, 
             const std::string /* user_profile */, 
             const time_t /* last_updated */);
  ~S3Uploader();
  void cancelUpload(void);
  
private:
  std::string   path_;
  std::string   user_profile_;
  time_t        last_updated_;
  std::mutex    cancel_mutex_;
  std::thread   main_thread_;
  bool          canceled_;
  
  void uploadFile(void);
};

#endif // S3UPLOADER_H
