#include "s3fs.h"
#include <syslog.h>
#include <string>
#include <cstring>
#include <cstdio>
#include <unistd.h>
#include <sys/types.h>
#include <utime.h>
#include <dirent.h>
#include <iostream>

int s3_access(const char *path, int mask)
{
  syslog(LOG_INFO, "%s | s3_access | %s\n", get_username(), path);
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  
  return (access(full_cache_path.c_str(), mask) != 0) ? -errno : 0;
}

int s3_mknod(const char *path, mode_t mode, dev_t dev)
{
  syslog(LOG_INFO, "%s | s3_mknod | %s\n", get_username(), path);
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  
  return (mknod(full_cache_path.c_str(), mode, dev) != 0) ? -errno : 0;
}

int s3_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
  syslog(LOG_INFO, "%s | s3_create | %s\n", get_username(), path);
  
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  pid_t process = fuse_get_context()->pid;
  std::set<std::string> files_;
  if (G_OPEN_FILES.count(process) > 0) {
    files_ = G_OPEN_FILES[process];
    files_.insert(full_cache_path);
    G_OPEN_FILES[process] = files_;
  } else {
    files_.insert(full_cache_path);
    G_OPEN_FILES.insert(std::make_pair(process, files_));
  }
  
  fi->fh = creat(full_cache_path.c_str(), mode);
  
  return ((signed) fi->fh < 0) ? -errno : 0;
}

int s3_open(const char *path, struct fuse_file_info *fi)
{
  /* We use the G_OPEN_FILES structure because we can not 
   * rely on the opening flags: O_RDONLY | O_WRONLY | O_RDWR */
  
  syslog(LOG_INFO, "%s | s3_open | %s\n", get_username(), path);

  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  pid_t process = fuse_get_context()->pid;
  
  if (G_OPEN_FILES.count(process) > 0) {
    std::set<std::string> files_ = G_OPEN_FILES[process];
    
    if (files_.find(std::string(full_cache_path)) == files_.end()) {
      /* The file has not been previously opened by the process, 
       * so, it can be downloaded if needed */
      struct stat st_path;
      int ret_stat = stat(full_cache_path.c_str(), &st_path);
      
      if ((ret_stat != 0 && errno == ENOENT) || 
          (ret_stat == 0 && st_path.st_size == 0) || 
          file_needs_update(path)) { 
        /* File does not exists locally OR it does exists but we have to 
         * download it for the first time OR we have to update the local file */
        int ret_val = download_file(path);
        if (ret_val != 0) return ret_val;
      }
      
      files_.insert(full_cache_path);
      G_OPEN_FILES[process] = files_;
    } 
  } else {
    std::set<std::string> files_;
    struct stat st_path;
    int ret_stat = stat(full_cache_path.c_str(), &st_path);
    
    if ((ret_stat != 0 && errno == ENOENT) || 
        (ret_stat == 0 && st_path.st_size == 0) || 
        file_needs_update(path)) { 
      /* File does not exists locally OR it does exists but we have to 
       * download it for the first time OR we have to update the local file */
      int ret_val = download_file(path);
      if (ret_val != 0) return ret_val;
    }
    
    files_.insert(full_cache_path);
    G_OPEN_FILES.insert(std::make_pair(process, files_));
  }
  
  /* Now, we can check if the file has been opened by another PID and 
   * we can ask the system if that PID is still running, if not, we can 
   * remove the PID from our map. -- Will this work when opening through
   * samba? */
  
  syslog(LOG_INFO, "I am going to open from cache\n");
  /* Now we can open it from cache dir */
  fi->fh = open(full_cache_path.c_str(), fi->flags);
  
  return ((signed) fi->fh < 0) ? -errno : 0;
}

int s3_write(const char *path, const char *buf, size_t size, 
                    off_t offset, struct fuse_file_info *fi)
{
  char * username = get_username();
  syslog(LOG_INFO, "%s | s3_write | %s\n", username, path);
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  struct stat st_path;
  stat(full_cache_path.c_str(), &st_path);
  
  int ret_val = pwrite(fi->fh, buf, size, offset);
  
  if (ret_val > 0) {
    G_S3_QUEUE.addFile(path, username, st_path.st_mtime);
  } else {
    // TODO log error IO
    ret_val = -errno;
  }
  
  return ret_val;
}

int s3_release(const char *path, struct fuse_file_info *fi)
{
  syslog(LOG_INFO, "%s | s3_release | %s\n", get_username(), path);

//   // TODO buscar el archivo en G_OPEN_FILES con el proceso 
//   std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
//   pid_t process = fuse_get_context()->pid;
//   std::set<std::string> files_;
//   if (G_OPEN_FILES.count(process) > 0) {
//     files_ = G_OPEN_FILES[process];
//     files_.insert(full_cache_path);
//     G_OPEN_FILES[process] = files_;
//   } else {
//     files_.insert(full_cache_path);
//     G_OPEN_FILES.insert(std::make_pair(process, files_));
//   }
  
  return (close(fi->fh) != 0) ? -errno : 0;
}

int s3_flush(const char *path, struct fuse_file_info *fi)
{
  syslog(LOG_INFO, "%s | s3_flush | %s\n", get_username(), path);
  
  return 0;
}

int s3_read(const char *path, char *buf, size_t size, off_t offset,
            struct fuse_file_info *fi)
{
  syslog(LOG_INFO, "%s | s3_read | %s\n", get_username(), path);
  int ret_val = pread(fi->fh, buf, size, offset);
  return (ret_val == -1) ? -errno : ret_val;
}

int s3_releasedir(const char *path, struct fuse_file_info *fi)
{
  syslog(LOG_INFO, "%s | s3_releasedir | %s\n", get_username(), path);
  
  return 0;
}

int s3_opendir(const char *path, struct fuse_file_info *fi)
{
  syslog(LOG_INFO, "%s | s3_opendir | %s\n", get_username(), path);
  
  return 0;
}

int s3_readdir(const char *path,  void *buf, fuse_fill_dir_t filler,
               off_t offset, struct fuse_file_info *fi)
{
  char * username = get_username();
  syslog(LOG_INFO, "%s | s3_readdir | %s\n", username, path);
  
  filler(buf, ".", NULL, 0);
  filler(buf, "..", NULL, 0);
  
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  if (strcmp(path, "/") != 0) full_cache_path += "/";
                                
  int diff_in_time = access_time_diff(full_cache_path.c_str());
  if (diff_in_time > CACHE_MAX_SECS) { /* We need to update from AWS */
    syslog(LOG_INFO, "I am going to do the request to AWS\n");
    std::string aws_bucket_path = G_BUCKET_NAME + path; 
    if (strcmp(path, "/") != 0) aws_bucket_path += "/";
    
    std::string aws_cmd = "aws s3 ls " + aws_bucket_path + " --profile " +
                          G_USER_PROFILES[std::string(username)];
                          
    syslog(LOG_INFO, aws_cmd.c_str());
    
    FILE * aws_cmd_fh = popen(aws_cmd.c_str(), "r");
    char line[AWS_MAX_FILENAME_LEN+64];
    while(fgets(line, AWS_MAX_FILENAME_LEN+64, aws_cmd_fh) != NULL)
    {
      /* Checking bad responses */
      if (strcmp(line, "None") == 0) { /* path does not exists */
        pclose(aws_cmd_fh);
        return 0;
      }
      
      /* Cleaning the spaces at the beggining */
      size_t line_len = strlen(line);
      unsigned i;
      for (i=0; i<line_len; i+=1) {
        if(!isspace(line[i])) break;
      }
      memmove(line, line+i, line_len-i);
      
      /* Getting the actual name of the file or dir */
      char * temporal_name = (char *) malloc(sizeof(*temporal_name)*(line_len-i));
      if (strncmp(line, "PRE", 3) == 0) { /* It is a directory */
        sscanf(line, "%*s %[^/]s\n", temporal_name);
        std::string full_path = full_cache_path + temporal_name;
        
        if (opendir(full_path.c_str()) == NULL) {
          if (errno == ENOENT) { /* Directory does not exists */
            if (mkdir(full_path.c_str(), S_IRWXU | S_IRWXG | S_IRWXO) != 0) { 
              /* Error creating the directory in cache*/
              syslog(LOG_INFO, "Error creating directory %s\n", 
                    full_path.c_str());
              pclose(aws_cmd_fh);
              return -1;
            }
          } else if(errno == ENOTDIR) { /* It is not a directory */
            syslog(LOG_INFO, "%s exists as a file not as a directory\n", 
                    full_path.c_str());
            pclose(aws_cmd_fh);
            return -1;
          }
        } else { /* It does exists, we need to update the access time */
          time_t current_time = time(NULL);
          std::string touch_cmd = "touch -a -d \"" 
                                  + std::string(ctime(&current_time)) 
                                  +"\" " + full_cache_path;
          system(touch_cmd.c_str());
        }
      } else { /* It is a file */
        // TODO sanitize the filename !! 
        
        /* Keep the date and the filename */
        char date_[15], time_[15];
        sscanf(line, "%s %s %*s %s\n", date_, time_, temporal_name);
        std::string touch_cmd = "touch -m -d \"" + std::string(date_) + " " 
                                + std::string(time_) +"\" " + full_cache_path
                                + temporal_name;
        
        system(touch_cmd.c_str()); /* This will create with RW permission */ 
      }
      
      free(temporal_name);
    }
    
    pclose(aws_cmd_fh);
  } 
  
  /* Now we can read from the cache dir */
  syslog(LOG_INFO, "I am going to read from the cache\n");
  
  DIR *dp = opendir(full_cache_path.c_str());
  struct dirent *de;
  de = readdir(dp);
  
  do {
    if (strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0) {
      filler(buf, de->d_name, NULL, 0);
    }
  } while ((de = readdir(dp)) != NULL);
  
  closedir(dp);
    
  return 0;
}

int s3_mkdir(const char *path, mode_t mode)
{
  syslog(LOG_INFO, "%s | s3_mkdir | %s\n", get_username(), path);
  
  return 0;
}

int s3_rmdir(const char *path)
{
  syslog(LOG_INFO, "%s | s3_rmdir | %s\n", get_username(), path);
  
  // TODO aws rm command
  
  return 0;
}

int s3_getattr(const char *path, struct stat *statbuf)
{
  int ret_val = 0;
  syslog(LOG_INFO, "%s | s3_getattr | %s\n", get_username(), path);
  
  memset(statbuf, 0, sizeof(struct stat));
  
  if (strlen(path) > 2 && path[1] == '.') {
    return 0;
  }
  
  if (strcmp(path, "/") == 0 || strcmp(path, ".") == 0) {
    statbuf->st_mode = S_IFDIR | 0755;
    statbuf->st_nlink = 2;
  } else { 
    /* build the full path tho the cache dir file */
    std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
    if ( stat(full_cache_path.c_str(), statbuf) != 0 ) { 
      /* The file does not exists locally, thus, 
       * we must check if it exists in AWS */
      bool folder = false;
      ret_val = aws_has_file(path, &folder);
      if (ret_val < 0) return ret_val; 
      
      std::string touch_cmd = "touch " + full_cache_path;
      if (folder) touch_cmd += "/";
      system(touch_cmd.c_str()); /* This will create with RW permission */
      ret_val = stat(full_cache_path.c_str(), statbuf);
    }
  }
  
  return ret_val;
}

int s3_readlink(const char *path, char *link, size_t size)
{
  syslog(LOG_INFO, "%s | s3_readlink | %s\n", get_username(), path);
  
  return 0;
}

int s3_ftruncate(const char *path, off_t offset, struct fuse_file_info *fi)
{
  syslog(LOG_INFO, "%s | s3_ftruncate | %s\n", get_username(), path);
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  
   /* Lets try to keep the last modification date */
  struct stat st_path;
  stat(full_cache_path.c_str(), &st_path);
  
  if (ftruncate(fi->fh, offset) != 0) return -errno;
  
  struct utimbuf newtimes;
  newtimes.actime  = st_path.st_atime;
  newtimes.modtime = st_path.st_mtime;
  utime(full_cache_path.c_str(), &newtimes);
  
  return 0;
}

int s3_fsync(const char *path, int datasync, struct fuse_file_info *fi)
{
  char * username = get_username();
  syslog(LOG_INFO, "%s | s3_fsync | %s\n", username, path);
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  struct stat st_path;
  stat(full_cache_path.c_str(), &st_path);
  
  int ret_val = fsync(fi->fh);
  if (ret_val == 0) {
    G_S3_QUEUE.addFile(path, username, st_path.st_mtime);
  } else {
    ret_val = -errno;
  }
  
  return ret_val;
}

int s3_link(const char *path, const char *newpath)
{
  syslog(LOG_INFO, "%s | s3_link | %s\n", get_username(), path);
  
  return 0;
}

int s3_unlink(const char *path)
{
  syslog(LOG_INFO, "%s | s3_unlink | %s\n", get_username(), path);
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  int ret_val = 0;
  
  /* We verify if the file exists in the queue 
   * to be uploaded and we remove it from the queue */
  G_S3_QUEUE.removeFile(full_cache_path);
  
  if (unlink(full_cache_path.c_str()) != 0) {
    ret_val = -errno;
  }
  
  // TODO aws rm command
  
  return ret_val;
}

int s3_rename(const char *path, const char *newpath)
{
  syslog(LOG_INFO, "%s | s3_rename | %s to %s\n", get_username(), path, newpath);
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  std::string full_cache_npath = G_WORK_DIR_PREFIX + G_MOUNTPOINT + newpath;
  
  return rename(full_cache_path.c_str(), full_cache_npath.c_str());
}

int s3_symlink(const char *path, const char *link)
{
  syslog(LOG_INFO, "%s | s3_symlink | %s\n", get_username(), path);
  
  return 0;
}

int s3_chown(const char *path, uid_t uid, gid_t gid)
{
  syslog(LOG_INFO, "%s | s3_chown | %s\n", get_username(), path);
  
  return 0;
}

int s3_utime(const char *path, struct utimbuf *ubuf)
{
  syslog(LOG_INFO, "%s | s3_utime | %s\n", get_username(), path);
  
  return 0;
}

int s3_chmod(const char *path, mode_t mode)
{
  syslog(LOG_INFO, "%s | s3_chmod | %s\n", get_username(), path);
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  
  return chmod(full_cache_path.c_str(), mode);
}

int s3_truncate(const char *path, off_t newsize)
{
  syslog(LOG_INFO, "%s | s3_truncate | %s\n", get_username(), path);
  std::string full_cache_path = G_WORK_DIR_PREFIX + G_MOUNTPOINT + path;
  
  /* Lets try to keep the last modification date */
  struct stat st_path;
  stat(full_cache_path.c_str(), &st_path);
  
  if (truncate(full_cache_path.c_str(), newsize) != 0) return -errno;
  
  struct utimbuf newtimes;
  newtimes.actime  = st_path.st_atime;
  newtimes.modtime = st_path.st_mtime;
  utime(full_cache_path.c_str(), &newtimes);
  
  return 0;
}

int s3_statfs(const char *path, struct statvfs *statv)
{
  syslog(LOG_INFO, "%s | s3_statfs | %s\n", get_username(), path);
  
  return 0;
}

int s3_fsyncdir(const char *path, int datasync, 
                struct fuse_file_info *fi) 
{
  syslog(LOG_INFO, "%s | s3_fsyncdir | %s\n", get_username(), path);
  
  return 0;
}

void *s3_init(struct fuse_conn_info *conn)
{
  syslog(LOG_INFO, "%s | s3_init \n", get_username());
  
  return 0;
}

void s3_destroy(void *userdata)
{
  syslog(LOG_INFO, "%s | s3_destroy \n", get_username());
}

int s3_fgetattr(const char *path, struct stat *statbuf, 
                struct fuse_file_info *fi)
{
  syslog(LOG_INFO, "%s | s3_fgetattr | %s\n", get_username(), path);
  int ret_val = 0;
  
  if ((signed) fi->fh < 0) {
    ret_val = s3_getattr(path, statbuf);
  } else {
    ret_val = fstat(fi->fh, statbuf);
  }
  
  return ret_val;
}

#ifdef HAVE_SYS_XATTR_H
int s3_setxattr(const char *path, const char *name, const char *value, 
                size_t size, int flags)
{
  syslog(LOG_INFO, "%s | s3_setxattr | %s\n", get_username(), path);
  
  return 0;
}

int s3_getxattr(const char *path, const char *name, char *value, size_t size)
{
  syslog(LOG_INFO, "%s | s3_getxattr | %s\n", get_username(), path);
  
  return 0;
}

int s3_listxattr(const char *path, char *list, size_t size)
{
  syslog(LOG_INFO, "%s | s3_listxattr | %s\n", get_username(), path);
  
  return 0;
}

int s3_removexattr(const char *path, const char *name)
{
  syslog(LOG_INFO, "%s | s3_removexattr | %s\n", get_username(), path);
  
  return 0;
}
#endif