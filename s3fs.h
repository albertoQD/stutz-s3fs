#ifndef STUTZ_S3FS_S3FS_H
#define STUTZ_S3FS_S3FS_H

#include "s3tools.h"
#include <sys/types.h>
#include <fuse.h>

int    s3_access(const char *, int);
int    s3_mknod(const char *, mode_t, dev_t);
int    s3_create(const char *, mode_t, struct fuse_file_info *);
int    s3_open(const char *, struct fuse_file_info *);
int    s3_write(const char *, const char *, size_t, off_t , 
                struct fuse_file_info *);
int    s3_release(const char *, struct fuse_file_info *);
int    s3_flush(const char *, struct fuse_file_info *);
int    s3_read(const char *, char *, size_t, off_t, struct fuse_file_info *);
int    s3_releasedir(const char *, struct fuse_file_info *);
int    s3_opendir(const char *, struct fuse_file_info *);
int    s3_readdir(const char *, void *, fuse_fill_dir_t, off_t, 
                  struct fuse_file_info *);
int    s3_mkdir(const char *, mode_t);
int    s3_rmdir(const char *);
int    s3_getattr(const char *, struct stat *);
int    s3_readlink(const char *, char *, size_t);
int    s3_ftruncate(const char *, off_t , struct fuse_file_info *);
int    s3_fsync(const char *, int, struct fuse_file_info *);
int    s3_link(const char *, const char *newpath);
int    s3_unlink(const char *);
int    s3_rename(const char *, const char *);
int    s3_symlink(const char *, const char *);
int    s3_chown(const char *, uid_t, gid_t);
int    s3_utime(const char *, struct utimbuf *);
int    s3_chmod(const char *, mode_t);
int    s3_truncate(const char *, off_t);
int    s3_statfs(const char *, struct statvfs *);
int    s3_fsyncdir(const char *, int, struct fuse_file_info *);
void * s3_init(struct fuse_conn_info *);
void   s3_destroy(void *);
int    s3_fgetattr(const char *, struct stat *, struct fuse_file_info *);

#ifdef HAVE_SYS_XATTR_H
int    s3_setxattr(const char *, const char *, const char *, size_t, int);
int    s3_getxattr(const char *, const char *, char *, size_t);
int    s3_listxattr(const char *, char *, size_t);
int    s3_removexattr(const char *, const char *);
#endif


#endif // S3FS_S3FS_H
