#define FUSE_USE_VERSION 26

#ifdef linux
/* For pread()/pwrite() */
#define _XOPEN_SOURCE 500
#endif 

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <utility>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <fuse.h>
#include <errno.h>
#include <sys/types.h>
#include <dirent.h>
#include <syslog.h>
#include <unistd.h>
#include <pwd.h>
#include "s3fs.h"

static struct fuse_operations s3_oper;

void set_fuse_operations(void)
{
  s3_oper.getattr      = s3_getattr,
  s3_oper.readlink     = s3_readlink,
  s3_oper.getdir       = NULL,
  s3_oper.mknod        = s3_mknod,
  s3_oper.mkdir        = s3_mkdir,
  s3_oper.unlink       = s3_unlink,
  s3_oper.rmdir        = s3_rmdir,
  s3_oper.symlink      = s3_symlink,
  s3_oper.rename       = s3_rename,
  s3_oper.link         = s3_link,
  s3_oper.chmod        = s3_chmod,
  s3_oper.chown        = s3_chown,
  s3_oper.truncate     = s3_truncate,
  s3_oper.utime        = s3_utime,
  s3_oper.open         = s3_open,
  s3_oper.read         = s3_read,
  s3_oper.write        = s3_write,
  s3_oper.statfs       = s3_statfs,
  s3_oper.flush        = s3_flush,
  s3_oper.release      = s3_release,
  s3_oper.fsync        = s3_fsync,
                           
  s3_oper.opendir      = s3_opendir,
  s3_oper.readdir      = s3_readdir,
  s3_oper.releasedir   = s3_releasedir,
  s3_oper.fsyncdir     = s3_fsyncdir,
  s3_oper.init         = s3_init,
  s3_oper.destroy      = s3_destroy,
  s3_oper.access       = s3_access,
  s3_oper.create       = s3_create,
  s3_oper.ftruncate    = s3_ftruncate,
  s3_oper.fgetattr     = s3_fgetattr;
  
#ifdef HAVE_SYS_XATTR_H  
  s3_oper.setxattr     = s3_setxattr,
  s3_oper.getxattr     = s3_getxattr,
  s3_oper.listxattr    = s3_listxattr,
  s3_oper.removexattr  = s3_removexattr,
#endif
}

int main(int argc, char **argv) 
{
    
  if (argc < 3) {
    std::cerr << "Usage: " << argv[0] << " <s3_bucket> <mountpoint>" << std::endl;
    exit(1);
  }
  
  G_MOUNTPOINT  = std::string(argv[2]);
  G_BUCKET_NAME = std::string(argv[1]);
  
  /* Sanitize args -- just triming the final slashes -- */ 
  if (*G_MOUNTPOINT.rbegin() == '/') {
    G_MOUNTPOINT = std::string(G_MOUNTPOINT.begin(), G_MOUNTPOINT.end()-1);
  }
  
  if (*G_BUCKET_NAME.rbegin() == '/') {
    G_BUCKET_NAME = std::string(G_BUCKET_NAME.begin(), G_BUCKET_NAME.end()-1);
  }
  
  /* Check the existence of <mountpoint> */
  if (opendir(G_MOUNTPOINT.c_str()) == NULL) {
    std::cerr << "Directory " << G_MOUNTPOINT << " does not exists" << std::endl;
    exit(1);
  }
  
  // TODO Check for the argument -work_dir
  
  create_cache_work_dir();
  
  /* Setting the environment variable to make sure the 
   * AWS CLI can find the config file */
  setenv("AWS_CONFIG_FILE", G_CONFIG_FILE.c_str(), 1); 
  /* Load the mapping of the system's users with the aws IAM users */ 
  init_aws_user_profiles();
  /* Define functions of fuse_operations */ 
  set_fuse_operations();
  
  /* Prepare args for fuse_main */
  argv[1] = const_cast<char *>(G_MOUNTPOINT.c_str());
  for (int i=2; i<argc; i+=1) argv[i] = argv[i+1];
  argc-=1;
  int ret_val = fuse_main(argc, argv, &s3_oper, NULL);
  clean_cache_work_dir();
  return ret_val;
}
