#include "s3uploader.h"
#include <unistd.h>
#include <iostream>
#include <cstdio>
#include <syslog.h>
#include <libgen.h>
#include <string>
#include "s3tools.h"

S3Uploader::S3Uploader(const std::string path,
                       const std::string user_profile,
                       const time_t last_updated)
: path_(path), 
  user_profile_(user_profile),
  last_updated_(last_updated),
  canceled_(false)
{
  main_thread_ = std::thread(&S3Uploader::uploadFile, this);
  main_thread_.detach();
}

S3Uploader::~S3Uploader()
{
}

void S3Uploader::cancelUpload(void)
{
  cancel_mutex_.lock();
    canceled_ = true;
  cancel_mutex_.unlock();
  std::cout << "***************************************************" << std::endl
            << ">>> Subida de archivo " << path_ << " cancelada" << std::endl
            << "***************************************************" << std::endl;
}

void S3Uploader::uploadFile()
{
  // Check for sleep function in seconds
  sleep(TIME_TO_SLEEP);
  
  cancel_mutex_.lock();
    if (canceled_) {
      cancel_mutex_.unlock();
      return;
    }
  cancel_mutex_.unlock();
  
  /* Do the actual uploading */
  std::cout << "***************************************************" << std::endl 
            << ">>> Deberia estar subiendo el archivo " << path_ << std::endl
            << "***************************************************" << std::endl;
  
  /* Validar las fechas del archivo local vs el remoto:
   *  - Si el remoto fue actualizado desde la ultima vez que lo baje se 
   *    cuentan cuantos archivos en conflictos tiene y se sube uno nuevo 
   *    (de conflicto).
   *  - Si no, se sube normal */
  
  char * ptr_path = const_cast<char *>(path_.c_str());
  std::string upload_filename = std::string(basename(ptr_path));
  std::string str_path  = std::string(dirname(ptr_path)) + "/";
  std::string fullpath = str_path + upload_filename;
  
  int conflicts = 0;
  if (outdated(fullpath.c_str(), user_profile_.c_str(), last_updated_)) {
    // count the number of files already in conflict for remote_filename_
    // and build an string for the new filename 
    std::vector<std::string> files;
    files = files_in_aws_path(str_path.c_str(), user_profile_.c_str());
    
    for (unsigned int i=0; i<files.size(); i+=1) {
      conflicts += (files[i].find(upload_filename) != std::string::npos);
    }
    
    upload_filename += "-CONFLICT" + std::to_string(conflicts);
    
    /* Here we set back the modication time for the file 
     * to the last_updated to ensure that we will keep 
     * having conflicts next time with the same process */
    std::string touch_cmd = "touch -m -d \"" 
                                  + std::string(ctime(&last_updated_)) 
                                  +"\" " + G_WORK_DIR_PREFIX + G_MOUNTPOINT
                                  + fullpath;
    system(touch_cmd.c_str()); /* This will create with RW permission */ 
  }
  upload_filename = str_path + upload_filename;
  upload_file(fullpath.c_str(), upload_filename.c_str(), 
              user_profile_.c_str(), conflicts == 0);
}
