#ifndef STUTZ_S3FS_S3QUEUE_H
#define STUTZ_S3FS_S3QUEUE_H

#include "s3uploader.h"
#include <string>
#include <map>

class S3Queue
{
public:
  S3Queue();
  ~S3Queue();
  
  void addFile(const std::string /* local_filename */, 
               const std::string /* user_profile */,
               const time_t /* last_updated */);
  void removeFile(const std::string /*filename*/);
  
private:
  std::map<std::string, S3Uploader*> upload_queue_;
};

#endif // S3QUEUE_H
