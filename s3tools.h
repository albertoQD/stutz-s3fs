#ifndef STUTZ_S3FS_S3TOOLS_H
#define STUTZ_S3FS_S3TOOLS_H

#ifdef linux
/* For pread()/pwrite() */
#define _XOPEN_SOURCE 500
#endif 

#define FUSE_USE_VERSION 26
#define AWS_MAX_FILENAME_LEN 1024
#define CACHE_MAX_SECS 5
#define TIME_TO_SLEEP 10

#include "s3queue.h"
#include <string>
#include <vector>
#include <set>
#include <map>

extern std::string G_CONFIG_FILE;
extern std::string G_USERS_CONFIG;
extern std::string G_WORK_DIR_PREFIX;
extern std::map<std::string, std::string> G_USER_PROFILES;
extern std::string G_BUCKET_NAME; 
extern std::string G_MOUNTPOINT;
extern S3Queue G_S3_QUEUE;
extern std::map<pid_t, std::set<std::string> > G_OPEN_FILES;

char*                    get_username(void);
int                      access_time_diff(const char *);
void                     init_aws_user_profiles(void);
void                     create_cache_work_dir(void);
void                     clean_cache_work_dir(void);
int                      evaluate_output(const char *, const char *);
int                      download_file(const char *);
void                     upload_file(const char *, const char *, 
                                     const char *, bool);
int                      aws_has_file(const char *, bool *);
bool                     file_needs_update(const char *);
bool                     outdated(const char *, const char *, time_t);
std::vector<std::string> files_in_aws_path(const char *, const char *);

#endif // S3TOOLS_H
