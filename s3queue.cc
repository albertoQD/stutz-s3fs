#include "s3queue.h"
#include <utility>

S3Queue::S3Queue()
{
  upload_queue_ = std::map<std::string, S3Uploader*>();
}

S3Queue::~S3Queue()
{

}

void S3Queue::addFile(const std::string local_filename, 
                      const std::string user_profile,
                      const time_t last_updated)
{
  this->removeFile(local_filename);
  upload_queue_.insert(std::make_pair(local_filename, new S3Uploader(
                                                            local_filename,
                                                            user_profile,
                                                            last_updated)));
  
}

void S3Queue::removeFile(const std::string filename)
{
  std::map<std::string, S3Uploader*>::iterator it;
  it = upload_queue_.find(filename);
  if (it != upload_queue_.end()) {
    ((*it).second)->cancelUpload();
  }
}
